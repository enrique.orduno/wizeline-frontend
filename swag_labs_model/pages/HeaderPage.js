import {Selector} from 'testcafe'

class HeaderPage {
    
    constructor() {
    // Locators
   // this.pageTitle          = Selector('.product_label')
    this.burgerMenu         = Selector('#react-burger-menu-btn')
    this.burgerMenuAllItems = Selector('#inventory_sidebar_link')
    this.burgerMenuAbout    = Selector('#about_sidebar_link')
    this.burgerMenuLogOut   = Selector('#logout_sidebar_link')
    this.burgerMenuResetApp = Selector('#reset_sidebar_link')
    this.shoppingCart       = Selector('.shopping_cart_link.fa-layers.fa-fw')
    this.shoppingCartBadge  = Selector('.fa-layers-counter.shopping_cart_badge')
    }
}
export default new HeaderPage()