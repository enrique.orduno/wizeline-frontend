import {Selector} from 'testcafe'

class ShoppingCartPage {
    
    constructor() {
    // Locators
    this.pageTitle        = Selector('.subheader').withText('Your Cart')
    this.quantity         = Selector('.cart_quantity_label')
    this.description      = Selector('.cart_desc_label')
    this.continueShopping = Selector('.btn_secondary').withText('Continue Shopping')
    this.checkout         = Selector('.btn_action.checkout_button').withText('CHECKOUT')
    }
}
export default new ShoppingCartPage()