import {Selector} from 'testcafe'

class Checkout_OverviewPage {
    
    constructor() {
    // Locators
    this.pageTitle = Selector('.subheader').withText('Checkout: Overview')
    this.itemList  = Selector('.inventory_item_name') 
    this.productName  = Selector('.inventory_item_name')
    this.cancelButton = Selector('.cart_cancel_link.btn_secondary').withText('CANCEL')
    this.finishButton = Selector('.btn_action.cart_button').withText('FINISH')
    }
}
export default new Checkout_OverviewPage()