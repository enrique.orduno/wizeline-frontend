import {Selector, t} from 'testcafe'

class LoginPage {
    
    constructor() {
    // Locators
    // this.pageTitle     = - not be used, will use instead the username Input Locator as it's unique throught the site
    this.userNameInput = Selector('#user-name')
    this.passwordInput = Selector('#password')
    this.LoginButton   = Selector('#login-button')
    this.errorButton   = Selector('h3[data-test=error]').withText('Username and password do not match any user in this service')
    }
    //Login Flow - common method for all tests
    async homeLogin(username,password) {
        await t
          .typeText(this.userNameInput, username)
          .typeText(this.passwordInput, password)
          .click   (this.LoginButton)
    }
}
export default new LoginPage()