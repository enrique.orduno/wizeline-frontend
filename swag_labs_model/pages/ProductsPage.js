import {Selector, t} from 'testcafe'

class ProductsPage {
    
    constructor() {
    // Locators
    this.pageTitle       = Selector('.product_label').withText('Products')
    this.addToCartButton = Selector('.btn_primary.btn_inventory') 
    this.productName     = Selector('.inventory_item_name') 
    }

    async addMultipleItems(numberOfItems) {
        if (numberOfItems>6){
        throw 'Currently the page only shows 6 items - You selected a value > 6 - TEST will HALT'
        }
        for (let i=0; i<numberOfItems; i++)
            await t.click(this.addToCartButton.nth(0))
    }
}
export default new ProductsPage()