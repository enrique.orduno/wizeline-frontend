import {Selector} from 'testcafe'

class FinishPage {
    
    constructor() {
    // Locators
    this.pageTitle           = Selector('.subheader').withText('Finish')
    this.confirmationMessage = Selector('.complete-header')
    }
}
export default new FinishPage()