import {Selector} from 'testcafe'

class Checkout_YourInformationPage {
    
    constructor() {
    // Locators
    this.pageTitle     = Selector('.subheader').withText('Checkout: Your Information Cart')
    this.firstName     = Selector('#first-name')
    this.lastName      = Selector('#last-name')
    this.zipCode       = Selector('#postal-code')
    this.cancelButton  = Selector('.btn_primary.cart_button')
    this.continueButton= Selector('.btn_primary.cart_button')
    this.errorMessage  = Selector('[data-test=error]')
    }
}
export default new Checkout_YourInformationPage()