// import pages to be used
import LoginPage    from '../pages/LoginPage.js'
import ProductsPage from '../pages/ProductsPage.js'
import ShoppingCartPage from '../pages/ShoppingCartPage.js'
import HeaderPage from '../pages/HeaderPage.js'
import Checkout_YourInformationPage from '../pages/Checkout_YourInformationPage.js'
import Checkout_OverviewPage from '../pages/Checkout_OverviewPage.js'
import FinishPage from '../pages/FinishPage.js'
// constants
import {CREDENTIALS} from '../data/Constants.js'
import {YOUR_INFORMATION} from '../data/Constants.js'
import {NUMBER_OF_ITEMS} from '../data/Constants.js'
import {CONFIRMATION_MESSAGE} from '../data/Constants.js'

fixture('Assignment 1 Tests').page`https://www.saucedemo.com/ `
//.beforeEach() - I would include the VALID login which is common to all pages except for Test #2, from there all tests diverisfy
//              - instead I created a method that takes care of the login for both cases.
test('1. Login with a VALID user', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.INVALID_USER.PASSWORD)
    await t.expect(ProductsPage.pageTitle.exists).ok()
})

test('2. Login with an INVALID user', async t => {
    await LoginPage.homeLogin(CREDENTIALS.INVALID_USER.USERNAME, CREDENTIALS.INVALID_USER.PASSWORD)
    await t.expect(LoginPage.errorButton.exists).ok()
})

test('3. Log out from Products Page', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    await t.click(HeaderPage.burgerMenu).click(HeaderPage.burgerMenuLogOut)
    await t.expect(LoginPage.userNameInput.exists).ok()
})

test('4. Navigate to the shopping cart', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    await t.click(HeaderPage.shoppingCart).expect(ShoppingCartPage.pageTitle.exists).ok()
})

/* TEST - adds a random single value from the available products page and then validates the shopping cart icon in the header appears with a '1' 
        -TODO - maybe a better way to do it, is to get into the cart and validate the name there.. test 9 does this. 
*/
test('5. Add a single item to the shopping cart', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    // Click random item from 1-6 options - but first obtain the number of products (in case the number changes in the future)
    const count = await ProductsPage.productName.count
    await t.click(ProductsPage.addToCartButton.nth(Math.floor(Math.random()*count)))
    await t.hover(HeaderPage.shoppingCartBadge)
    await t.expect(HeaderPage.shoppingCartBadge.withText('1').exists).ok()
})
/* Test - Adds the number of products pre-defined in the Constants.js files with 'NUMBER_OF_ITEMS'
      - Test doesn't specify to get inside the shopping cart and check each product name it can be done, this test is just a quick 
      - check to make sure the cart is adding up the products
*/
test('6. Add multiple items to the shopping cart', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    await ProductsPage.addMultipleItems(NUMBER_OF_ITEMS)
    await t.hover(HeaderPage.shoppingCartBadge)
    await t.expect(HeaderPage.shoppingCartBadge.withText(NUMBER_OF_ITEMS).exists).ok()
})

/* OPTION A - Randomizes the field to be left empty and 'expects' error message depending on the field that was left empty
  -'Mail' or 'User' information?
*/
test('7. OPTION A - Continue with missing mail information -  randomize missing field', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    await t.click(ProductsPage.addToCartButton.nth(Math.floor(Math.random()*6)))
    await t.click(HeaderPage.shoppingCart)
    await t.click(ShoppingCartPage.checkout)
    // Fill out Form
    // Randomize field to be empty / Fields accept ' ' (space) as a valid value, so just passing an empty value to typeText didn't worked
    let information = Object.keys(YOUR_INFORMATION)
    let randomField = information[Math.floor(Math.random()*3)]

    switch (randomField) {
        case 'FIRST_NAME': await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME) 
                           await t.typeText(Checkout_YourInformationPage.zipCode, YOUR_INFORMATION.ZIP_CODE)
                           await t.click(Checkout_YourInformationPage.continueButton)
                           await t.expect(Checkout_YourInformationPage.errorMessage.withText('First Name').exists).ok()
                            break
        case 'LAST_NAME' : await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME) 
                           await t.typeText(Checkout_YourInformationPage.zipCode, YOUR_INFORMATION.ZIP_CODE)
                           await t.click(Checkout_YourInformationPage.continueButton)
                           await t.expect(Checkout_YourInformationPage.errorMessage.withText('Last Name').exists).ok()
                            break
        case 'ZIP_CODE'  : await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME) 
                           await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME)
                           await t.click(Checkout_YourInformationPage.continueButton)
                           await t.expect(Checkout_YourInformationPage.errorMessage.withText('Postal Code').exists).ok()
                            break
        default          : console.log('No valid value provided')
    }
})
// OPTION B - Hard code the Field you want to leave empty (by just not interacting with the element) and just expect a general error message 
test('7. OPTION B - Continue with missing mail information - hard code missing field', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    const count = await ProductsPage.productName.count
    await t.click(ProductsPage.addToCartButton.nth(Math.floor(Math.random()*count)))
    await t.click(HeaderPage.shoppingCart)
    await t.click(ShoppingCartPage.checkout)
    await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME)
    await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME)
    // Do not provide zip code to make the form submission fail
    await t.click(Checkout_YourInformationPage.continueButton)
    // Expect General error message, not specifying exact expected value
    await t.expect(Checkout_YourInformationPage.errorMessage.exists).ok()
})

test('8. Fill users information', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    const count = await ProductsPage.productName.count
    await t.click(ProductsPage.addToCartButton.nth(Math.floor(Math.random()*count)))
    await t.click(HeaderPage.shoppingCart)
    await t.click(ShoppingCartPage.checkout)
    // Fill out Form
    await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME)
    await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME)
    await t.typeText(Checkout_YourInformationPage.zipCode, YOUR_INFORMATION.ZIP_CODE)
    await t.click(Checkout_YourInformationPage.continueButton)
    await t.expect(Checkout_OverviewPage.pageTitle.exists).ok()
})

test('9. Final order items', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    const count = await ProductsPage.productName.count
    const randomProduct = Math.floor(Math.random()*count)
    const productName   =  await ProductsPage.productName.nth(randomProduct).innerText
    await t.click(ProductsPage.addToCartButton.nth(randomProduct))
    await t.click(HeaderPage.shoppingCart)
    await t.click(ShoppingCartPage.checkout)
    
    await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME)
    await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME)
    await t.typeText(Checkout_YourInformationPage.zipCode, YOUR_INFORMATION.ZIP_CODE)
    await t.click(Checkout_YourInformationPage.continueButton)
    await t.expect(Checkout_OverviewPage.pageTitle.exists).ok()
    // Validate random 'productName' exists in OverviewPage
    await t.expect(Checkout_OverviewPage.itemList.withText(productName).exists).ok()
})

test('10. Complete a purchase', async t => {
    await LoginPage.homeLogin(CREDENTIALS.VALID_USER.USERNAME, CREDENTIALS.VALID_USER.PASSWORD)
    const count = await ProductsPage.productName.count
    const randomProduct = Math.floor(Math.random()*count)
    const productName   =  await ProductsPage.productName.nth(randomProduct).innerText
    
    await t.click(ProductsPage.addToCartButton.nth(randomProduct))
    await t.click(HeaderPage.shoppingCart)
    await t.click(ShoppingCartPage.checkout)
    // Fill out Form
    await t.typeText(Checkout_YourInformationPage.firstName, YOUR_INFORMATION.FIRST_NAME)
    await t.typeText(Checkout_YourInformationPage.lastName, YOUR_INFORMATION.LAST_NAME)
    await t.typeText(Checkout_YourInformationPage.zipCode, YOUR_INFORMATION.ZIP_CODE)
    await t.click(Checkout_YourInformationPage.continueButton)
    await t.expect(Checkout_OverviewPage.pageTitle.exists).ok()
    // Validate random 'productName' exists in OverviewPage
    await t.expect(Checkout_OverviewPage.itemList.withText(productName).exists).ok()
    await t.click(Checkout_OverviewPage.finishButton)
    await t.expect(FinishPage.confirmationMessage.withText(CONFIRMATION_MESSAGE).exists).ok()
})