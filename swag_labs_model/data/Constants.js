// variables to export to our tests
export const CREDENTIALS = {
    VALID_USER : {
        USERNAME : 'standard_user', 
        PASSWORD : 'secret_sauce'
    },
    INVALID_USER : {
        USERNAME : 'invalid_user',
        PASSWORD : 'secret_sauce'
    },
    INVALID_USER_PASSWORD : {
        USERNAME : 'invalid_user',
        PASSWORD : 'invalid_password'
    }
}

export const YOUR_INFORMATION = {
    FIRST_NAME : 'Enrique',
    LAST_NAME  : 'Orduno',
    ZIP_CODE   : '22506'
}

export const NUMBER_OF_ITEMS = '6'
export const CONFIRMATION_MESSAGE = 'THANK YOU FOR YOUR ORDER'